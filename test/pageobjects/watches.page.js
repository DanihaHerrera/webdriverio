const Page = require ('./page');

class WatchesPage extends Page{
    get banner(){ return $('section.b-promobanner'); }
    get infoBannerTitle() { return $('.b-promobanner__info-title'); }
    get bannerBtn() { return $('.b-promobanner__info-btn'); }
    get url() { return browser.getUrl();}
    get watchesCategoryList() { return $$('section[id="s0-16-13-0-1[0]-0-0"] ul li');}
    get FashionLink() {return $$('.hl-cat-nav__js-tab a[href*="Fashion"]')[0];}
    get WatchesLink() { return $('.hl-cat-nav__sub-cats a[href*="Watches-Parts"]');}
    open(){
        super.open('/');
    }

    getwatchesCategoryListText() { 
        const watchesList = [];
        this.watchesCategoryList.map((element) => 
        watchesList.push(element.getText())
        );
        return watchesList;
    }
}
module.exports = new WatchesPage();