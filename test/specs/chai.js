const should = require ('chai/register-should');

const expectChai = require('chai').expect
const assert = require('chai').assert


describe('EBay Watches Page', () => {
    it('Should show the Promo banner container', () => {
       browser.url('https://www.ebay.com/b/Watches-Parts-Accessories/260324/bn_2408535');
       const banner = $('section.b-promobanner');
        expect(banner).toBeDisplayed();
    });

    it('Should show the banner title', () => {
        const infoBannerTitle = $('.b-promobanner__info-title');
        const infoBannerTitleText = infoBannerTitle.getText();
        expect(infoBannerTitle).toHaveTextContaining('Rolex');
        expectChai([infoBannerTitleText]).to.not.be.empty;
        infoBannerTitle.should.not.be.empty;
        assert.isNotEmpty(infoBannerTitleText);
    });

    it('Should cointain link on banner button  and verify its clickable', () => {
        const bannerBtn = $('.b-promobanner__info-btn');
        const tag = bannerBtn.getTagName();
        expect(bannerBtn).toHaveLinkContaining('/fashion/');
        expect(bannerBtn).toBeClickable();
        expectChai(tag).to.equal('a');
        tag.should.equal('a');
    });

    it('Should click on banner button  and verify the new url', () => {
        const bannerBtn = $('.b-promobanner__info-btn');
        bannerBtn.click();
        const url = browser.getUrl();
        expectChai(url).to.include('/fashion/') ;

        expect(browser).toHaveUrl('https://www.ebay.com/e/fashion/rolex-021720');
    });
});