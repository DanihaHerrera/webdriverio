const expectChai = require('chai').expect
const WatchesPage = require('../pageobjects/watches.page')
const waitAndClick = require("../utilities/helper");
const resources = require('../resources').default;


describe('EBay Watches Page', () => {
    before(() => {
        WatchesPage.open();
        WatchesPage.FashionLink.moveTo();
        //browser.pause(1000);
        waitAndClick(WatchesPage.WatchesLink, 5000)
        //WatchesPage.WatchesLink.waitForDisplayed({timeout:1000})
        ///WatchesPage.WatchesLink.click();

    });
    

    it('Should verify the watches category list', () => {
        const watchesCategoryList = WatchesPage.getwatchesCategoryListText();
        expectChai(watchesCategoryList).to.deep.equal(resources.watchesCategoryList);
    });

    it('Should show the Promo banner container', () => {
       expect(WatchesPage.banner).toBeDisplayed();
    });

    it('Should show the banner title', () => {
        expect(WatchesPage.infoBannerTitle).toHaveTextContaining('Rolex')
    });

    it('Should cointain link on banner button  and verify its clickable', () => {
        expect(WatchesPage.bannerBtn).toHaveLinkContaining('/fashion/')
        expect(WatchesPage.bannerBtn).toBeClickable()
    });

    it('Should click on banner button  and verify the new url', () => {
        WatchesPage.bannerBtn.click()
        expectChai(WatchesPage.url).to.include('/fashion/') 

        expect(browser).toHaveUrl('https://www.ebay.com/e/fashion/rolex-021720')
    });

    
});