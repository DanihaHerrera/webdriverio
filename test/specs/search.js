const  SearchPage  = require("../pageobjects/search.page");
const waitForTextChange = require("../utilities/helper");
const resources = require('../resources').default;
const  addSeverity = require('@wdio/allure-reporter').default
const  addFeature  = require('@wdio/allure-reporter').default
const allureReporter = require('@wdio/allure-reporter').default 
describe('Ebay Product Search', () => {
    it('Should open the main url and verify the title', () => {
        SearchPage.open();
        expect(browser).toHaveTitle(resources.homeTitle);
    });

    it('Should search for a prpduct and verify the search text value', () => {
        allureReporter.addSeverity('Critical');
        SearchPage.searchInput.addValue('Laptop');
        SearchPage.searchButton.click(); 
        expect(SearchPage.searchInput).toHaveValue(resources.laptopTitle);
    });

    it('Should redirect to a new page and verify the title', () =>{
        expect(browser).toHaveTitle('laptop | eBay')
    }); 

    it('Should update the search category',() => {
        allureReporter.addFeature('Search Category');
        waitForTextChange(SearchPage.category, 'PC Laptops & Netbooks', 10000) ;
        expect(SearchPage.category).toHaveText('PC Laptops & Netbooks');
    });
});
