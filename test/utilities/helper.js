module.exports = waitForTextChange = (el, text, timeout) => {
    browser.waitUntil(
        function (){
            return el.getText() === (text);
        },
        {
            timeout
        }
    );
};

module.exports = waitAndClick = (el, timeout) => {
    el.waitForDisplayed({ timeout});
    el.click();
};